import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { PreguntasService } from './pregunta/preguntas.service';
import { Ipeguntas } from './pregunta/ipeguntas';
import { Observable, config } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { DialogRespuestaComponent } from './pregunta/dialog-respuesta/dialog-respuesta.component';
import { MatButtonToggle, MatButtonToggleGroup } from '@angular/material/button-toggle';
@Component({
  selector: 'app-viewpreuntas',
  templateUrl: './viewpreuntas.component.html',
  styleUrls: ['./viewpreuntas.component.scss']
})
export class ViewpreuntasComponent {

  listarpreguntas$!: Observable<Ipeguntas[]>;
  @ViewChild('toggleGroup') toggleGroup!: MatButtonToggleGroup;
  @ViewChildren('toggles') toggles!: QueryList<MatButtonToggle>;

  seccionAbierta: number | null = null;

  valorgrupo: any = false;

  cantidadpreguntas:number = 0;

  constructor(
    private _formBuilder: FormBuilder,
    private preguntasservice: PreguntasService,
    private dialog: MatDialog
  ) {

    this.listarpreguntas$ = preguntasservice.ObtenerListarPreguntas();
     this.listarpreguntas$.subscribe( s => [...s].forEach((item,index)=>{
      this.cantidadpreguntas++;
      }))
    
     


  }



  //firstFormGroup: FormGroup = this._formBuilder.group({firstCtrl: ['']});

  calificar(idpregunta: number, pregunta: string, respuesta: boolean, seleccion: string) {

    this.preguntasservice.SetRespuesta(idpregunta, pregunta, respuesta, seleccion);


  }

  openDialog() {
    this.dialog.open(DialogRespuestaComponent, {
      width: '800px'


    });

  }

  resetRespuestas() {

    this.preguntasservice.restRespustas();
  }

  resetToggle(): void {
    this.toggleGroup.value = null; // o un valor por defecto
    this.valorgrupo = null;
  }


  
  toggle(index: number) {
   
    this.seccionAbierta = this.seccionAbierta === index ? null : index;
   
  }

 
     
     
    
  


}
