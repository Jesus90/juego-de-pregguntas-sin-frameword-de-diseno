import { Component } from '@angular/core';
import { PreguntasService } from '../preguntas.service';
import { Irespuestas } from '../../respuestas/irespuestas';



@Component({
  selector: 'app-dialog-respuesta',
  templateUrl: './dialog-respuesta.component.html',
  styleUrls: ['./dialog-respuesta.component.scss']
})
export class DialogRespuestaComponent {

  listarRespuestas!: Irespuestas[];

  cuntLementos: number = 0;



  constructor(private preguntasservice: PreguntasService) {

    this.listarRespuestas = this.preguntasservice.getRespuesta();

    this.cuntLementos = this.listarRespuestas.length;
  }


}
