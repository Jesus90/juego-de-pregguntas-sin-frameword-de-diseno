import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewpreuntasComponent } from './viewpreuntas.component';

describe('ViewpreuntasComponent', () => {
  let component: ViewpreuntasComponent;
  let fixture: ComponentFixture<ViewpreuntasComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ViewpreuntasComponent]
    });
    fixture = TestBed.createComponent(ViewpreuntasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
