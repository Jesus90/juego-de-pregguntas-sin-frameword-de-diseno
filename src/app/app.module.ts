import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedMaterialModule } from './core/shared.material/shared.material.module';
import { ViewpreuntasComponent } from './main/viewpreuntas/viewpreuntas.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogRespuestaComponent } from './main/viewpreuntas/pregunta/dialog-respuesta/dialog-respuesta.component';

@NgModule({
  declarations: [
    AppComponent,
    ViewpreuntasComponent,
    DialogRespuestaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
